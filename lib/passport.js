const Passport = require('passport').Passport
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const { User } = require('../models')
require("dotenv").config();

const options = {
   jwtFromRequest: ExtractJwt.fromHeader('authorization'),
   secretOrKey: process.env.JWT_SECRET,
}

const passportPlayer = new Passport();
const passportAdmin = new Passport();

passportAdmin.use(new JwtStrategy(options, async (payload, done) => {
   console.log('admin', payload)
   User.findOne({
      where: {
         id: payload.id,
         role: 2,
      },
   }).then(user => done(null, user))
      .catch(err => done(err, false))
}))

passportPlayer.use(new JwtStrategy(options, async (payload, done) => {
   User.findOne({
      where: {
         id: payload.id,
         role: 1,
      },
   }).then(user => done(null, user))
      .catch(err => done(err, false))
}))

module.exports = {
   passportPlayer,
   passportAdmin,
}