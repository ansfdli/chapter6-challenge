'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGameBiodata.belongsTo(models.UserGame, {foreignKey: 'user_id'});
    }
  };
  UserGameBiodata.init({
    fullname: DataTypes.STRING,
    biodata: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserGameBiodata',
  });
  return UserGameBiodata;
};

// 'use strict';

// module.exports = {
//   up: async (queryInterface, Sequelize) => {
//     const transaction = await queryInterface.sequelize.transaction();
//     try {
//       await queryInterface.addColumn(
//         'UserGameHistories',
//         'user_id',
//         Sequelize.INTEGER
//       );
//       await queryInterface.addConstraint('UserGameHistories', {
//         type: 'foreign key',
//         fields: ['user_id'],
//         name: 'usergame_usergamehistory_id_fkey',
//         references: {
//           table: 'UserGames',
//           field: 'id',
//         },
//         onDelete: 'CASCADE',
//         transaction
//       });
//       return transaction.commit();
//     } catch (error) {
//       await transaction.rollback();
//       throw error;
//     }
//   },

//   down: async (queryInterface, Sequelize) => {
//     const transaction = await queryInterface.sequelize.transaction();
//     try {
//       await queryInterface.removeConstraint(
//         'UserGameHistories',
//         'usergame_usergamehistory_username_fkey',
//         { transaction }
//       );
//       await queryInterface.removeColumn(
//         'UserGameHistories',
//         'user_id'
//       );
//       return transaction.commit();
//     } catch (error) {
//       await transaction.rollback();
//       throw error;
//     }
//   }
// };