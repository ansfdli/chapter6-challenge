const express = require('express');
const path = require("path");
const router = require('./router/router');
const authRouter = require('./router/authRouter');
const app = express();

const {passportAdmin, passportPlayer} = require('./lib/passport')
app.use(passportAdmin.initialize())
app.use(passportPlayer.initialize())

require("dotenv").config();

const port = process.env.API_GATEWAY_PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const db = require("./models/index");
const NEVER_CHANGE = { force: false };
db.sequelize.sync(NEVER_CHANGE);

app.set("view engine", "ejs");
app.set('views', path.join(__dirname, '/views'));
app.use(router);
app.use(authRouter);

app.listen(port, () => console.log(`App running at port ${port}`));

// sequelize model:generate --name UserGame --attributes username:string,password:string