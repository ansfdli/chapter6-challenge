const { User, room, History, sequelize } = require('../models');

let playedFirst = '';
let playedFirstInput = '';
let round = 1;
let resultArray = [];

function suit(player, comp) {
    if (player === comp) return "draw"

    else if (player === "gunting") {

        if (comp === "kertas") return "player 1"
        else if (comp !== "kertas") return "player 2"

    }
    else if (player === "batu") {
        return comp === "kertas" ? "player 2" : "player 1"
    }
    else if (player === "kertas") {
        return comp === "batu" ? "player 1" : "player 2"
    }
}
const parse = (data) => {
    const res = {};
    data.forEach((item) => {
        Object.keys(item).map(key => {
            if (!res[key]) {
                res[key] = item[key]
            } else {
                res[key] += item[key]
            }
        })
    })
    return res;
}

module.exports = {
    // register process
    register: (req, res, next) => {
        //  { username, password } in register params in user-jwt-bcrypt
        user.register(req.body)
            .then((cek) => {
                if (cek) {
                    res.redirect('/api/login')
                } else {
                    res.send({ message: "something wrong" })
                }
            })
            .catch(err => next(err))
    },

    // login process
    login: (req, res) => {

        User.authenticate(req.body)
            .then(authResult => {
                const { id, username } = authResult
                const data = {
                    id,
                    username,
                    token: authResult.generateToken()
                }
                res.send(data)

            })
            .catch(err => {
                res.send({ message: err.message });
            })

    },
    loginAdmin: (req, res) => {

        User.authenticate(req.body)
            .then(authResult => {
                const { id, username } = authResult
                const data = {
                    id,
                    username,
                    token: authResult.generateToken()
                }
                res.redirect('/dashboard')

            })
            .catch(err => {
                res.send({ message: err.message });
            })

    },
    whoami: async (req, res) => {
        const currentUser = req.user;
        const { id, username } = currentUser;
        const history = await History.getHistory(id);
        res.send({ id, username, history })
    },
    createRoom: (req, res) => {
        room.generate(req.body.name)
            .then(_room => {
                res.send(_room)
            })
            .catch(err => {
                res.send({ message: `${err.message}. Maybe name is duplicate.` });
            })
    },
    fight: (req, res) => {
        const currentPlayer = req.user;
        if (playedFirst) {
            if (currentPlayer.id === playedFirst.id && playedFirstInput) {
                res.send(`kamu (id: ${currentPlayer.id}, username: ${currentPlayer.username}) udah main`)
            } else {
                const playedSecondInput = req.body.option;
                let hasilSuit = suit(playedFirstInput, playedSecondInput)
                if (hasilSuit === "draw") {
                    resultArray.push({
                        [playedFirst.id]: 0,
                        [currentPlayer.id]: 0
                    })
                } else if (hasilSuit === "player 1") {
                    resultArray.push({
                        [playedFirst.id]: 1,
                        [currentPlayer.id]: 0
                    })
                } else if (hasilSuit === "player 2") {
                    resultArray.push({
                        [playedFirst.id]: 0,
                        [currentPlayer.id]: 1
                    })
                }

                playedFirst = '';
                playedFirstInput = '';
                if (round === 3) {
                    round = 1;
                    const cleanData = parse(resultArray);
                    const { roomID } = req.params;
                    let keys = Object.keys(cleanData)
                    sequelize.transaction(t => {
                        return History.create({
                            player_id: parseInt(keys[0]),
                            room_id: parseInt(roomID),
                            result: cleanData[keys[0]]
                        }, { transaction: t }).then(() => {
                            return History.create({
                                player_id: parseInt(keys[1]),
                                room_id: parseInt(roomID),
                                result: cleanData[keys[1]]
                            }, { transaction: t });
                        })
                    }).then(() => {
                        const temp = resultArray;
                        resultArray = [];
                        temp.push({ winner_id: cleanData[keys[1]] > cleanData[keys[0]] ? parseInt(keys[1]) : parseInt(keys[0]) })
                        res.send(temp)
                    }).catch(err => {
                        console.log('error transaksi', err)
                        res.send(err)
                    });
                } else {
                    round++;
                    res.send(resultArray)
                }
            }
        }
        else {
            playedFirst = currentPlayer;
            playedFirstInput = req.body.option;
            res.send('waiting for other player input')
        }
    },
}