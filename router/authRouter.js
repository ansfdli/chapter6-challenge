const router = require('express').Router();
const auth = require('../controllers/authController');
const { restrictAdmin, restrictPlayer } = require('../middlewares/restrict');

router.get('/dashboard', restrictAdmin, (req, res) => res.render('dashboard')); 

router.use(restrictPlayer)
router.get('/api/auth/whoami', auth.whoami)
router.post('/api/auth/create-room', auth.createRoom)
router.post('/api/auth/fight/:roomID', auth.fight)

module.exports = router;